\documentclass[12pt]{article}
\usepackage[paper=a4paper,left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}

\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\usepackage[polish]{babel}

\title{Uczenie Maszynowe}
\author{Yulii Ivanov, trzeci rok Informatyki}
\date{June 2020}

\begin{document}

\maketitle

\section*{Pytania z pierwszego rozdziału książki
Aurélien Géron:\\,,Uczenie maszynowe z użyciem Scikit-Learn i TensorFlow''.}
\begin{enumerate}
\item
Podaj definicję uczenia maszynowego.

\textbf{Odpowiedź:}
Uczenie maszynowe - jest to rodzaj sztucznej inteligencji, 
który sprawia, że system uczy się dzięki wykorzystaniu danych,
a nie jest programowany przez człowieka.

\item
Wymień cztery rodzaje problemów, z których rozwiązaniem najlepiej sobie radzi proces uczenia
maszynowego.

\textbf{Odpowiedź:}
\begin{enumerate}
    \item Nadzorowanie jezior danych
\item Analizy ogromnych ilości danych
\item Przetworzenie ogromnej ilości nieoznaczonych danych
\item Zapewnienie dokładnych wyników i analizy
\end{enumerate}


\item
Czym jest oznakowany zbiór danych uczących?

\textbf{Odpowiedź:}
Brak.

\item
Jakie są dwa najczęstsze zastosowania uczenia nadzorowanego?

\textbf{Odpowiedź:}
\begin{enumerate}
    \item Dla określanie, na przykład, poziomu ryzyka finansowego związanego z osobami i firmami na podstawie wcześniejszych informacji o ich działaniach.
    \item Wykorzystując analizę wcześniejszych wzorców zachowań, 
może dostarczyć przydatnych informacji, jak klienci będą się najprawdopodobniej zachowywać i jakie są ich preferencje.
\end{enumerate}

\item
Wymień cztery najpowszechniejsze zastosowania uczenia nienadzorowanego.

\textbf{Odpowiedź:}
Przykłady zastosowania uczenia nienadzorowanego: 
rozpoznawanie podobnych obiektów, wykrywania spamu, określanie uczuć i identyfikacji stanu emocjonalnego.

\item
Jakiego rodzaju algorytmu uczenia maszynowego użyłabyś/użyłbyś w robocie przeznaczonym
do poruszania się po nieznanym terenie?

\textbf{Odpowiedź:}
Algorytm uczenia przez wzmacnianie.

\item
Jakiego rodzaju algorytmu uczenia maszynowego użyłabyś/użyłbyś do rozdzielenia klientów
na kilka różnych grup?


\textbf{Odpowiedź:}
Algorytm klastrowania.

\item
Czy problem wykrywania spamu stanowi część mechanizmu uczenia nadzorowanego lub
nienadzorowanego?

\textbf{Odpowiedź:}
Uczenia nienadzorowanego.

\item
Czym jest system uczenia przyrostowego?

\textbf{Odpowiedź:}
Uczenie przyrostowe - to jest system trenowany na bieżąco poprzez sekwencyjne dostarczanie danych.
System uczenie przyrostowego można pokazać na przykładzie gry na plansze i podzielić na etapy, że:
\begin{enumerate}
    \item uczy się grać na plansze;
\item uzupełnia braki wiedzy;
\end{enumerate}

ma motywację, która prowadzi:
\begin{enumerate}
    \item szybkie uczenie się małych problemów;
\item mechanizmów wykorzystywanego przez ludzi;
i ma skalowalną architekturę sieci neuronowych, t.j. wykazuje korelację pomiędzy wynikami.
\end{enumerate}



\item
Czym jest uczenie pozakorowe?

\textbf{Odpowiedź:}
Uczenie pozakorowe - jest systemem trenującym poza jądrem Systemu uczenia przyrostowego z ang. out-of core learning. 
Wygląda to tak, że następuje wczytanie części danych i trenowanie systemu za ich pomocą, po czym cały
proces zostanie powtórzony dla całego zbioru danych uczących.

\item
W jakim algorytmie uczenia maszynowego jest wymagana miara podobieństwa do uzyskiwania
prognoz?

\textbf{Odpowiedź:}
 Algorytm uczenia drzew decyzyjnych.

\item
Wyjaśnij różnicę pomiędzy parametrem modelu a hiperparametrem algorytmu uczącego.

\textbf{Odpowiedź:}
Hiperparametry definiujemy już przed procesem uczenia, a 
parametry są wyprowadzane w procesie uczenia.

\item
Czego poszukują algorytmy uczenia z modelu? Z jakiej strategii najczęściej korzystają? W jaki
sposób przeprowadzają prognozy?

\textbf{Odpowiedź:}
Brak.

\item
Wymień cztery główne problemy związane z uczeniem maszynowym.

\textbf{Odpowiedź:}
\begin{enumerate}
    \item Zbyt mała lub zbyt duża zależność systemu od środowiska,
w którym się znajduje co może prowadzić do niepełnej analizy danych 
lub błędnej interpretacji,
\item Wiarygodność i poprawność generowanych wniosków, wiedza zdobyta w wyniku obserwacji ma charakter jedynie domyślny,
a rozumowanie indukcyjne nie może być w pełni udowodnione, a jedynie sfalsyfikowane,
\item Niekompletne lub częściowo sprzeczne dane,
\item Niezdefiniowanie ograniczeń dziedzinowych,
może prowadzić do zbyt daleko idących uogólnień i błędnych wniosków.
\end{enumerate}


\item
Z czym mamy do czynienia, jeżeli model sprawuje się znakomicie wobec danych uczących,
ale nie radzi sobie z uogólnianiem wobec nowych próbek? Wymień trzy możliwe rozwiązania.

\textbf{Odpowiedź:}
Brak.

\item
Czym jest zbiór testowy i dlaczego powinniśmy z niego korzystać?

\textbf{Odpowiedź:}
Zbiór testowy jest jednym z podzbiorów metody wydzielenia, 
która polega ona na podziale zbioru danych na dwa podzbiory -
jednym z których jest zbiór testowy. Zbiór testowy wykorzystywany 
jest do oszacowania uczącego się modelu i ostatecznego oszacowania skuteczności modelu.

\item
Do czego służy zbiór walidacyjny?

\textbf{Odpowiedź:}
Zbiór walidacyjny jest drugim z etapów metody wydzielania. 
Ten zbiór służy do weryfikacji skuteczności modelu i do określenia jakości modelu już w trakcie jego uczenia, dopiero po nim w
momencie osiągnięcie satysfakcjonujących wyników wykorzystuje się zbiór 
testowy do oszacowania skuteczności modelu.


\item
Co nam grozi w przypadku strojenia hiperparametru wobec zbioru testowego?

\textbf{Odpowiedź:}
Może grozić nam niesuksesywny wynik w przypadku, jeżeli proces strojenia hiperparametru jest wykonywany przez zbiór testowy,ponieważ będzie służył do uzyskania oceny siły predykcyjnej wybranego hiperparametru, a w przypadku, 
gdyby proces strojenia hiperparametru był wykonywany przez zbiór walidacyjny,
to on by posłużył najlepszemu doborowi hiperparametru.

\item
Czym jest sprawdzian krzyżowy oraz dlaczego warto go stosować wobec zbioru walidacyjnego?

\textbf{Odpowiedź:}
Sprawdzian krzyżowy jest techniką pozwalającą na oszacowanie skuteczności zestawu danych, który może być wielokrotnie 
podzielony na zbiory treningowe i zbiory walidacjne przez sprawdzian krzyżowy,
co daje możliwość zapobiec problemów przetrenowania modelu lub niedopasowania.
Sprawdzian krzyżowy warto stosować wobec zbioru walidacyjnego, bo zbiór danych walidacjny 
zapewnia bezstronnej oceny dopasowania modelu na zbiorze treningowym podczas strojenia modeli hiperparametrów.

\end{enumerate}

\end{document}
