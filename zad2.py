from keras.models import Sequential
from keras.layers.core import Dense
from keras.optimizers import SGD
import numpy as np

# funkcja AND

X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
y = np.array([[1], [0], [1], [1]])

model = Sequential()
model.add(Dense(1, activation="sigmoid"))

model.compile(loss='binary_crossentropy', optimizer=SGD(lr=0.1))

model.fit(X, y, batch_size=1, epochs=100)
print(model.predict(X))
